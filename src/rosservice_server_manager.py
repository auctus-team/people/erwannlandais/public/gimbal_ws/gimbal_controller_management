#!/usr/bin/env python3

import geometry_msgs.msg
from math import pi
from std_msgs.msg import String, MultiArrayDimension, Float64

from std_srvs.srv import SetBool

import rospy

import actionlib
import math

import numpy as np

from msg_srv_action_gestion.srv import GetFloat, GetFloatRequest, GetFloatResponse, SetString, GetBool, GetBoolRequest, DoubleFloat64MultiArraySrv, DoubleFloat64MultiArraySrvRequest, Float64MultiArraySrv, Float64MultiArraySrvRequest, SetString, SetStringRequest
from msg_srv_action_gestion.msg import poseVelAcc

from std_srvs.srv import SetBool

from sensor_msgs.msg import JointState

import tf2_ros
import geometry_msgs.msg

import roslaunch

from tf_management_pkg.tf_utils import tfUtils

from ros_wrap.com_manager import comManager


class RosserviceServerManagement:
    """!
    
    Only goal of node : manage 2nd experiment (view on object, saving of datas)

        Poss 1 : 

            - Get /stop_trigger call
            - When got this call : 
                - Call to stop FOV
            - If InRecord:
                - Call to stop .csv recording of simMark (if it exists)

        Poss 2 :
            - Get /timeout call (from FOV)
            - When got this call : 
                - Call to reboot
            - If InRecord :
                - Call to stop  .csv recording of simMark (if it exists)

        Poss 3 :
            - Get time call (rostopic)
            - If already in recording (state = InRecord): 
                - Call to stop .csv recording
                - state = "IDLE"
            - Call to start .csv recording
            - state = "InRecord".

    """
    def get_timer_call(self,msg):
        if self.inRecord:
            self.state = "RebootRec"
        else:
            self.state = "StartRec"
        self.SMS = 0
        self.activeTimer = True

    def get_timeout(self,req):

        self.state = "timeoutStop"
        self.SMS = 0

        return(True)

    def get_gcm_state(self,msg):

        self.gcm_state = msg.data


    def manage_stop_timer(self,req):
        ##  - first, call /stop_timer_fov
        ##  - get answer
        ##  - set flag to stop record
        ##  - return answer
        req2 = GetFloatRequest()
        ret = GetFloatResponse()
        suc = False
        while not rospy.is_shutdown() and not suc:
            suc,resp,ret = self.cM.call_service("stop_timer_fov",self.stop_fov,req2)
            self.r.sleep()
        if suc:
            self.state = "StopTimer"
            self.SMS = 0
            self.stopTime = ret.data.data

        else:

            ret= GetFloatResponse()
            ret.success = False

        return(ret)





    def __init__(self):
        a = 0
        rospy.init_node('rosservice_management',
                    anonymous=True)
        nodeName = rospy.get_name()
        nodeName+="/"
        self.r = rospy.Rate(10.0)

        self.possible_states = ["IDLE","StopTimer","StartRec","StopRec","RebootRec","timeoutStop"]
        self.state = "IDLE"

        self.SMS = 0

        self.activeTimer = False
        self.inRecord = False
  
        self.tU = tfUtils(True,False)

        self.cM = comManager()

        self.instructions_types = {
            "SIM":["simMark"],
            "PHY":["IMU","Opti"]

        }

        self.stopTime = -1


        ### only used to get some useful informations about our robot


        ### Init variables

        self.instruct_frame = rospy.get_param("/")

        self.joint_names = []

        ## Check which modality is called (IMU, MV, OP)
        pth = "/global_params/" 
        self.instruct = rospy.get_param(pth+"control_instruct","")
        self.instruct_fam = ""
        validInstruct = False
        if self.instruct != "":
            for key in self.instructions_types:
                if self.instruct in self.instructions_types[key]:
                    validInstruct = True
                    self.instruct_fam = key

        
        if not validInstruct:

            s = "ERROR : COMMAND INSTRUCTION IS INVALID. VALID INSTRUCTIONS ARE :"
            for key in self.instructions_types:
                for k in range(len(self.instructions_types[key])):
                    s+= " %s ;"%(self.instructions_types[key][k])

            s += " GIVEN INSTRUCTION : %s"%(self.instruct)
            rospy.logerr(s)
            exit()

        ## Publishers
            
        self.pub_stop_time = rospy.Publisher("/timer_infos", Float64)

        ## Subscribers

        self.curStatePub = rospy.Subscriber("/get_manager_state",String, self.get_gcm_state)
        self.gcm_state = ""


        ## Clients
        canRec = False
        
        if self.instruct_fam == "SIM":
            canRec = True
            ## simMark case
            ## true : start csv recording
            ## false : stop csv recording
            self.csv_recorder = rospy.ServiceProxy("/manage_csv_recording", SetBool)
            ## Subscribers
        self.timeSub = rospy.Subscriber("/start_timer",Float64,self.get_timer_call)        

        self.state_change = rospy.ServiceProxy('/management_state',SetString)
        self.stop_fov = rospy.ServiceProxy('/stop_timer_fov',GetFloat)

        ## Server
        ## will send "Reboot" if timeout
        self.timeout_recorder = rospy.Service('/timeout_warning',SetString, self.get_timeout)

        ## need to :
        ##  - first, call /stop_timer_fov
        ##  - get answer
        ##  - set flag to stop record
        ##  - return answer
        self.stop_trig = rospy.Service('/stop_timer',GetFloat, self.manage_stop_timer)
      


        while not rospy.is_shutdown():

            if self.state == "StartRec":

                if canRec:
                    suc,resp,ret = self.cM.call_service("manage_csv",self.csv_recorder,True)
                    if suc:
                        self.state = "IDLE"
                        self.inRecord = True

                else:
                    self.state = "IDLE"

            
            if self.state == "StopRec":
                if canRec:
                    suc,resp,ret = self.cM.call_service("manage_csv",self.csv_recorder,False)
                    if suc:
                        self.state = "IDLE"
                        self.inRecord = False

                else:
                    self.state = "IDLE"

            if self.state == "RebootRec":
                if canRec:
                    if self.SMS==0:
                        suc = True
                        if self.inRecord:
                            suc,resp,ret = self.cM.call_service("manage_csv",self.csv_recorder,False)
                        if suc:
                            self.SMS = 1
                            self.inRecord = False

                    elif self.SMS==1:
                        suc,resp,ret = self.cM.call_service("manage_csv",self.csv_recorder,True)
                        
                        if suc:
                            self.state = "IDLE"
                            self.SMS = 0
                            self.inRecord = True

                else:
                    self.state = "IDLE"

            if self.state == "timeoutStop":

                if self.SMS == 0:
                    req = SetStringRequest()
                    req.data = "Reboot"
                    suc,resp,ret = self.cM.call_service("management_state",self.state_change,req)

                    if suc:
                        self.SMS+=1
                        self.activeTimer = False

                elif self.SMS == 1:
                    suc = True
                    if canRec:
                        suc,resp,ret = self.cM.call_service("manage_csv",self.csv_recorder,False)
                    if suc:
                        self.state = "IDLE"   
                        self.SMS = 0

            if self.state == "StopTimer":

                if self.SMS == 0:
                    ## publish stop data
                    msg = Float64(self.stopTime)
                    self.pub_stop_time.publish(msg)        
                    if self.activeTimer:
                        self.SMS+=1
                    else:
                        self.state = "IDLE"   
                        self.SMS = 0

                elif self.SMS == 1:
                    ## set managerState as StopTimer (for good interface setting)
                    req = SetStringRequest()
                    req.data = "StopTimer"
                    print("call for stop")
                    suc,resp,ret = self.cM.call_service("management_state",self.state_change,req)
                    if suc:
                        self.SMS+=1
                
                elif self.SMS == 2:
                    ## wait to get confirm 
                    if self.gcm_state == "StopTimer":
                        ## activate timeoutStop procedure
                        ## (same procedure to stop interface)                        
                        self.SMS = 0
                        self.state = "timeoutStop"

            self.r.sleep()
        
        print("PCM Stopped!")




if __name__ == "__main__":
    RosserviceServerManagement()