#!/usr/bin/env python3

import geometry_msgs.msg
from math import pi
from std_msgs.msg import String, MultiArrayDimension

from std_srvs.srv import SetBool

import rospy

import actionlib
import math

import numpy as np

from msg_srv_action_gestion.srv import SetFloat, SetFloatRequest, SetStringWR, SetStringWRRequest, SetString, GetBool, GetBoolRequest, DoubleFloat64MultiArraySrv, DoubleFloat64MultiArraySrvRequest, Float64MultiArraySrv, Float64MultiArraySrvRequest, SetString, SetStringRequest
from msg_srv_action_gestion.msg import poseVelAcc

from std_srvs.srv import SetBool
from gazebo_msgs.srv import DeleteModel

from sensor_msgs.msg import JointState

import tf2_ros
import geometry_msgs.msg

import roslaunch

from tf_management_pkg.tf_utils import tfUtils

from ros_wrap.com_manager import comManager


class gimbalControllerManagement:

    def ask_pause_continue(self,msg):
        self.isPaused = msg.data
        print("Gimbal controller manager isPaused : ", self.isPaused)
        return(True,"")

    def getCorrConfirm(self,msg):
        corr_conf = msg.data.split("/")
        if len(corr_conf) == 2:
            if corr_conf[0] == self.instruct_frame:
                self.gotCorrConfirm = True

    def __init__(self):
        a = 0
        rospy.init_node('gimbal_controller_management',
                    anonymous=True)
        nodeName = rospy.get_name()
        nodeName+="/"
        self.r = rospy.Rate(10.0)

        ## quitBoundaries : automatically activates itself if abs(q[0]) == 6.28
        self.possible_states = ["quitBoundaries","IDLE","Init","Stopped","Reboot","StopTimer","Sleep", "ChangeEE"]
        self.state = "IDLE"
        self.isControllerOperational = False

        self.needQBClean = False
        self.savedState = ""
        self.savedRegTask = -1
        self.QStop = False
        self.savedTF = ""

        self.stateMachineState = 0
        self.isPaused = False

        self.M_br_ee = np.identity(4)
        self.ee_init_msg = Float64MultiArraySrvRequest()
        self.q_init_msg = JointState()

        self.takeSpeed = False
        self.curSpeed = []

        self.tU = tfUtils(True,False)

        self.cM = comManager()

        self.instructions_types = {
            "SIM":["simMark"],
            "PHY":["IMU","Opti"]

        }

        self.jv1_val = 0.0
        self.jv1_index = 0


        ### only used to get some useful informations about our robot


        ### Init variables

        self.instruct_frame = rospy.get_param("/")

        self.joint_names = []

        ## Check which modality is called (IMU, MV, OP)
        pth = "/global_params/" 
        self.instruct = rospy.get_param(pth+"control_instruct","")
        self.instruct_fam = ""
        validInstruct = False
        if self.instruct != "":
            for key in self.instructions_types:
                if self.instruct in self.instructions_types[key]:
                    validInstruct = True
                    self.instruct_fam = key

        
        if not validInstruct:

            s = "ERROR : COMMAND INSTRUCTION IS INVALID. VALID INSTRUCTIONS ARE :"
            for key in self.instructions_types:
                for k in range(len(self.instructions_types[key])):
                    s+= " %s ;"%(self.instructions_types[key][k])

            s += " GIVEN INSTRUCTION : %s"%(self.instruct)
            rospy.logerr(s)
            exit()



        self.isSim = rospy.get_param(nodeName + "sim", True)

        self.varPath = rospy.get_param(nodeName + "varPath","/gimbal/velocity_control/")

        self.init_joint = rospy.get_param(self.varPath+"q_init",[])




        self.ang_error = np.zeros((3,1))

        globalPathParams = "/global_params/"

        self.global_ref = rospy.get_param(globalPathParams+"global_frame","world")
        self.opti_base_ref = rospy.get_param(globalPathParams+"base_ref_master","detromp")

        self.robot_base_ref = rospy.get_param(nodeName+"robot_frame","base")   

        self.tFCorrName = rospy.get_param(nodeName+"tfCorrName","centerBoundTF")
        ## to suppress if next lines are working
        #self.robot_pose_ref = rospy.get_param(nodeName+"init_pose_frame","des_0_frame")   
        #self.master_SM_ref = "M"+self.robot_pose_ref


        self.robot_pose_ref = rospy.get_param(globalPathParams+"origin_ref_frame_slave","des_0_frame")   
        self.master_SM_ref = rospy.get_param(globalPathParams+"origin_ref_frame_master","Mdes_0_frame")   


        self.slave_side_ref = rospy.get_param(globalPathParams+"ref_frame_slave","cam_frame")
        self.instruct_frame = rospy.get_param(globalPathParams+"tracked_frame_master","vM_frame")   

        self.thresError = rospy.get_param(nodeName + "thres_error", 1e-1)

        self.thresArtError = rospy.get_param(nodeName + "thres_art_error", 1e-1)


        ### Pub / Sub definition

        self.jointComPub = rospy.Publisher("/JSD",JointState)
        self.curStatePub = rospy.Publisher("/get_manager_state",String)

        self.errorSub = rospy.Subscriber("/QP_errors",poseVelAcc,self.get_errors)


        self.corrComPub = rospy.Publisher("/tfCorrOrder", String)

        self.corrComConfirm = rospy.Subscriber("/tfCorrConfirm", String, self.getCorrConfirm)
        self.gotCorrConfirm = False

        if self.isSim:
            self.jss =rospy.Subscriber("/gimbal/joint_states",JointState,self.getCurState)

        else:            
            self.jss =rospy.Subscriber("/joint_states",JointState,self.getCurState)
        
        ## Servers
        self.service_state = rospy.Service('/management_state',SetString, self.set_state)
        self.pause_continue = rospy.Service('/pause_continue_gcm',SetBool, self.ask_pause_continue)
        self.manager_state_ask = rospy.Service("/ask_for_manager_state",SetStringWR, self.ask_for_manager_state)

        ## Clients
        self.reset_num_turns = rospy.ServiceProxy("/reset_num_turns_servo", SetString)
        self.set_track_mvt = rospy.ServiceProxy('/set_track_mvt', SetBool)
        self.get_qp_init = rospy.ServiceProxy('/qp_is_initialized', GetBool)        
        self.get_pose_init = rospy.ServiceProxy('/get_pose_init', DoubleFloat64MultiArraySrv)        
        self.set_control_type = rospy.ServiceProxy("/set_control_type",SetFloat)
        if (self.instruct == "simMark"):
            #self.simMarkReset = rospy.ServiceProxy("/vialPoseFromFloatArray",Float64MultiArray)
            self.simMarkReset = rospy.ServiceProxy("/vialPoseFromTF",SetString)

        self.set_reg_task = rospy.ServiceProxy('/set_reg_task', DoubleFloat64MultiArraySrv)        

        self.set_tf_msr = rospy.ServiceProxy("/master_tracked_frame_msr",SetStringWR)
        self.reset_zero_axis = rospy.ServiceProxy("/reset_zero_axis",SetBool)

        ## For backToOrigin
        self.originState = -1

        ## For joint reaching
        self.num_k = 0
        self.failureRecovery = False

        self.prevState = ""

        self.begTime = -1

        while not rospy.is_shutdown():
            if (not self.isPaused):

                if self.state == "Init":
                    self.init_state_management()

                elif self.state == "Reboot" or self.state == "Stopped":
                    self.manage_analysis_end()

                elif self.state == "Sleep":
                    verbose = True
                    Ljoint = [0.0,0.0,0.0]
                    self.manage_specific_joint_configuration(Ljoint)
                    
                    if verbose and self.stateMachineState == 0:
                        rospy.loginfo("\\\\\\ PLATFORM IS READY TO SLEEP.")                    
                
                elif self.state == "ChangeEE":
                    Ljoint = [0.0,1.57078,0.0]
                    self.manage_specific_joint_configuration(Ljoint)
                
                elif self.state == "quitBoundaries":
                    self.manage_zero_arti_1()

            if self.prevState != self.state:
                ## publish currentState
                msg = String()
                msg.data = self.state
                self.curStatePub.publish(msg)

                self.prevState = self.state

            self.tU.updateSTS()
                
            self.r.sleep()
        
        print("PCM Stopped!")



    def ask_for_manager_state(self,msg):
        return(True,self.state)

    def getCurState(self,msg):

        if len(self.joint_names) == 0:
            self.joint_names = ["" for x in msg.name]
            nJ = len(msg.name)
            for k in range(nJ):
                #self.joint_names[nJ-k-1] = msg.name[k]
                self.joint_names[k] = msg.name[k]   
                if msg.name[k][-1] == "1":
                    self.jv1_index = k
            self.curSpeed = [-1 for k in range(nJ)]         


            if len(self.init_joint) == 0:
                self.init_joint = [0.0 for k in range(len(msg.name))]

        elif self.takeSpeed:
            for k in range(len(msg.velocity)):
                self.curSpeed[k] = msg.velocity[k]
        

        self.jv1_val = msg.position[self.jv1_index]
        #print(self.jv1_val)




    def do_handeye_calibration(self):
        """!
        
        Articular : 
        joint1 : -3.14; 3.14
        joint2 : -2.3; 2.3
        joint3 : -3.14;3.14
        
        """
        a = 1

    def get_errors(self,msg):
        self.ang_error[0] = msg.pose.orientation.x
        self.ang_error[1] = msg.pose.orientation.y
        self.ang_error[2] = msg.pose.orientation.z     


    def manage_specific_joint_configuration(self, Ljoint, verbose = True):
        """!
        
        Sleep mode : put platform back to sleep.
        Useful when you want to cut the platform

        """
        if verbose:
            rospy.loginfo("---- CURRENT STATE : %i ------"%self.stateMachineState)


        if self.stateMachineState == 0 :
            self.q_init_msg = JointState()
            for i in range(len(self.joint_names)):
                self.q_init_msg.name.append(self.joint_names[i])
                self.q_init_msg.position.append(Ljoint[i])
                self.q_init_msg.velocity.append(0.0)                    
            self.stateMachineState+=1
        
        if self.stateMachineState == 1:
            self.back_to_origin_management()

            if self.originState == -1:
                self.stateMachineState+=1

        if self.stateMachineState == 2:
            for i in range(len(self.joint_names)):
                self.q_init_msg.position[i] = self.init_joint[i]
            self.concludeState()

    def manage_zero_arti_1(self):
        """!
        
        Idea :
            * Stop qontrol
            * Ask to set regularization task to midOfJoints
                * qontrol should have weight for this
            * Activates manage_specific_pose_config
            * Reset regularization task to previous task
            * Reset new TF
            * Reset qontrol
        
        If new state :
            - If after 2nd step :  
                * Stop qontrol
                * Reset regularization task
                * Reset qontrol
        
        """

        #print("==== STATE : ", self.stateMachineState)
        if self.stateMachineState == 0:
            ## Met track_mvt à false
            success,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,False)
            if success:
                self.stateMachineState+=1
                self.QStop = True
        
        # elif self.stateMachineState == 1:
        #     ## Change reg task
        #     msg = DoubleFloat64MultiArraySrvRequest()
        #     ## code for center of boundaries
        #     msg.input.data.append(1)
        #     success,resp1,ret = self.cM.call_service("set_reg_task", self.set_reg_task,msg)
        #     if success:
        #         self.savedRegTask = ret.output.data[0]
        #         self.stateMachineState +=1
        
        elif self.stateMachineState == 1 or self.stateMachineState == 2:

            self.manage_specific_pose_configuration(self.tFCorrName, 1)
            if self.originState == 4 and self.stateMachineState == 1:
                ## Change reg task
                msg = DoubleFloat64MultiArraySrvRequest()
                ## code for center of boundaries
                msg.input.data.append(1)
                success,resp1,ret = self.cM.call_service("set_reg_task", self.set_reg_task,msg)
                if success:
                    self.savedRegTask = ret.output.data[0]
                    self.stateMachineState +=1
            

            elif self.originState == -1:
                self.stateMachineState=3
        
        elif self.stateMachineState == 3:
            suc = True
            if self.savedRegTask!= -1:
                ## Reset reg task
                msg = DoubleFloat64MultiArraySrvRequest()
                ## code for center of boundaries
                msg.input.data.append(self.savedRegTask)
                suc,resp1,ret = self.cM.call_service("set_reg_task", self.set_reg_task,msg)            
            if suc:
                self.stateMachineState+=1
        
        elif self.stateMachineState == 4:
            ## reset qontrol
            success,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,True)
            if success:
                self.concludeState()
                if self.needQBClean:
                    self.state = self.savedState
                self.savedState = ""
                self.savedRegTask = -1
                self.QStop = False
                self.savedTF = ""

    def manage_specific_pose_configuration(self, tf_name, cond=0, verbose = True):
        """!
        Cond : 
            = 0 : reach TF
            = 1 : wait for jv1 to be inferior to a value.

        
        Idea : 
            * Stop qontrol (if asked) (= trackMvt = false)
            * Switch cartesian
            * Ask master_slave_rebroadcaster to follow new TF, called tf_name (it should have be defined in the referentiel used 
            for link between master/slave part, in master part)
            * Get previous tf from master_slave_rebroadcaster
            * Activate qontrol
            * Wait for condition (reach tf_name, conditions on joint, ...)
            * Stop qontrol
            * Ask master_slave_reboradcaster to follow again new TF (if asked)
            * Reset qontrol (if asked)
    

        If new state : 
            If after step 3 :
                - Stop qontrol
                - Ask master_slave to follow again old TF
                - Reset qontrol (if asked)
            Else:  
                - Do nothing

                
        """


        if self.originState == -1:
            self.originState = 0

        #print("ORIGIN STATE : ", self.originState)

        if self.originState == 0:
            ##  Stop qontrol (if asked) (= trackMvt = false)
            suc = True
            if not self.QStop:
                suc,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,False)
            if suc:
                self.originState += 1
        
        elif self.originState == 1:
            ## Ask master_slave_rebroadcaster to follow new TF, called tf_name (it should have be defined in the referentiel used 
            ## for link between master/slave part, in master part)
            req = SetStringWRRequest()
            req.data = tf_name
            suc,resp1,ret = self.cM.call_service("set_tf_msr", self.set_tf_msr,req)
            if suc :
                self.savedTF = ret.result
                self.originState+=1

        elif self.originState == 2:
            ##  Switch controller à cartesian (= 0)
            req = SetFloatRequest()
            req.data.data = 0.0
            success,resp1,ret = self.cM.call_service("set_controller", self.set_control_type,req)
            if success:
                self.originState += 1

        elif self.originState == 3:
            ##  Reset qontrol 
            suc,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,True)
            if suc:
                self.originState += 1

        elif self.originState == 4:
            if cond == 0:
                max = 0.0
                for i in range(3):
                    if max < abs(self.ang_error[i]):
                        max = abs(self.ang_error[i])
                if max < self.thresError:
                    self.originState+=1 
            elif cond == 1:
                #print(self.jv1_val)
                if abs(self.jv1_val) < self.thresArtError:
                    self.originState+=1

        elif self.originState == 5:
            ##  Stop qontrol
            suc,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,False)
            if suc:
                self.originState += 1
            
        elif self.originState == 6:
            ## Ask master_slave_rebroadcaster to switch to previous TF
            suc = True
            if self.savedTF != "":
                req = SetStringWRRequest()
                req.data = self.savedTF
                suc,resp1,ret = self.cM.call_service("set_tf_msr", self.set_tf_msr,req)
            if suc :
                if not self.QStop:
                    self.originState+=1  
                else:
                    self.originState = -1

        elif self.originState == 7:
            ##  Reset qontrol
            suc,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,True)
            if suc:
                self.originState = -1            


    def back_to_origin_management(self,verbose = False):
        """
            * Met track_mvt à false
                ==> Appliqué à qontrol
            • Switch controller à joint
            * Envoi commande = q_init
            * track_mvt = True
            * Tq QP_error > thres, attend
            * Lorsque QP_error < thres, remet simMarker à pose_init (si existe)
            * track_mvt = False
            * Switch controller à pose

        """
        if verbose:
            rospy.loginfo("---- CURRENT B2O STATE : %i ------"%self.originState)

        if self.originState == -1:        
            self.originState = 0

        if self.originState == 0:
            ## Met track_mvt à false
            success,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,False)
            if success:
                if self.isSim:
                    self.originState+=2
                else:
                    self.originState+=1

        elif self.originState == 1:
            ## special real case : 
            ## reset last servo joint num_turns
            ## (to avoid to turn for a really
            ## long time for no reason)
 
            ## NOT TESTED YET, SO COMMENTED FOR NOW
            req = SetStringRequest()
            req.data = "gimbal_joint3"
            success,resp1,ret = self.cM.call_service("reset last joint num turns", self.reset_num_turns, req, verbose=verbose)
            
            #success = True
            if success:
                self.originState+=1

        elif self.originState == 2:
            ##  Switch controller à joint (= 1)
            req = SetFloatRequest()
            req.data.data = 1.0
            success,resp1,ret = self.cM.call_service("set_controller", self.set_control_type,req)
            if success:
                if self.isSim:
                    self.originState+=2
                else:
                    self.originState+=1
           

        elif self.originState == 3:
            ##   Envoi commande = q_init
            self.q_init_msg.header.stamp = rospy.Time.now()
            self.jointComPub.publish(self.q_init_msg)
            #rospy.sleep(0.1)
            ## Met track_mvt à True
            success,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,True)
            if success:
                self.originState+=1   

        elif self.originState == 4:
            ##   Tq QP_error > thres, attend
            self.q_init_msg.header.stamp = rospy.Time.now()
            self.jointComPub.publish(self.q_init_msg)
            max = 0.0
            for i in range(3):
                if max < abs(self.ang_error[i]):
                    max = abs(self.ang_error[i])
            if max < self.thresError:
                self.originState+=1       

        elif self.originState == 5:
            ## Met track_mvt à false
            success,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,False)
            if success:
                self.originState+=1

        elif self.originState == 6:
            ## Gère cas lorsque QP_error < thres
            if (self.instruct in ["Opti","IMU"]):
                ## Pas d'opération particulière dès maintenant, sera géré plus tard
                self.originState+=1
            
            elif self.instruct == "simMark":
                ## remet simMarker à pose_init (si existe)
                req = SetStringRequest()
                req.data = self.master_SM_ref
                success,resp1,ret = self.cM.call_service("reset_sim_mark", self.simMarkReset,req)
                if success:
                    self.originState+=1

        elif self.originState == 7:
            ## Met track_mvt à true (pour réenregistrer nvelle pose à atteindre)
            success,resp1,ret = self.cM.call_service("set_track_mvt", self.set_track_mvt,True)

            if success:
                self.originState=-1 
        

    def concludeState(self):
        self.originState = -1
        self.stateMachineState=0
        self.state = "IDLE"

    def manage_analysis_end(self, verbose = False):
        """!
        
        * Applique back_to_origin
        * Switch state from "Reboot" to "Stopped"
        * Attend que Rviz demande une mise à "Continue"
        * Remet track_mvt à True
        * State = "IDLE"
        
        """

        if verbose:
            rospy.loginfo("---- CURRENT AE STATE : %i ------"%self.stateMachineState)

        if self.stateMachineState == 0 :
            self.back_to_origin_management(True)

            if self.originState == -1:
                self.state = "Stopped"
                self.stateMachineState+=1

        elif self.stateMachineState == 1:
            if self.state == "Reboot":
                if self.instruct == "IMU":
                    ## corrige orientation
                    msg = String("%s/%s/%s"%(self.instruct_frame,self.master_SM_ref,self.slave_side_ref))
                    self.gotCorrConfirm = False
                    self.corrComPub.publish(msg)
                    self.stateMachineState+=1

                

                elif self.instruct in ["simMark","Opti"]:
                    ## Toutes opérations déjà gérées, on peut passer à la suite
                    self.stateMachineState+=2

        elif self.stateMachineState == 2:
            ## wait for confirm of correction (for IMU only)
            if self.gotCorrConfirm:
                self.stateMachineState+=1


        elif self.stateMachineState == 3:
            ## reset corr axis
            success,resp1,ret = self.cM.call_service("reset_zero_axis", self.reset_zero_axis,True)
            if success:
                self.stateMachineState+=1
        elif self.stateMachineState == 4:

            ## Réautorise cartesian control (=0.0)
            req = SetFloatRequest()
            req.data.data = 0.0
            success,resp1,ret = self.cM.call_service("set_controller", self.set_control_type,req)
            if success:
                self.concludeState()




            

    def init_state_management(self,verbose = True):
        """!
        
        * Attend que QP soit à état "Initialized"
        * Recup joint_names from joint_state_subscriber
        * Recup pose_init, réexprime ds world
        * Applique back_to_origin
        * track_mvt = True
        * State = "IDLE"
        
        """
        if verbose:
            rospy.loginfo("---- CURRENT INIT STATE : %i ------"%self.stateMachineState)


        if self.stateMachineState == 0:
            ## Attend que QP soit à état "Initialized"
            msg = GetBoolRequest()
            success,resp1,ret = self.cM.call_service("is_qp_init", self.get_qp_init, msg)
            if success and ret.data:
                #rospy.sleep(0.2)

                self.stateMachineState+=1

        if self.stateMachineState == 1:
            ## Check qu'on ait bien q_init
            if len(self.joint_names) > 0:
                ## Cree msg q_init
                for i in range(len(self.joint_names)):
                    self.q_init_msg.name.append(self.joint_names[i])
                    self.q_init_msg.position.append(self.init_joint[i])
                    self.q_init_msg.velocity.append(0.0) 

                # print(self.q_init_msg)
                # input()                   

                #self.stateMachineState+=1
                self.takeSpeed = True

                self.stateMachineState+=1



        elif self.stateMachineState == 2:
            ### Recup pose_init ds base_robot
            msg = DoubleFloat64MultiArraySrvRequest()
            success,resp1,ret = self.cM.call_service("get_init_pose", self.get_pose_init,msg)
            if success:
                L = ret.output.data
                # print(L)
                # s = input("WT")
                ## test
                self.robot_base_ref = "world"
                ## publish this transform as a reference static transform
                static_transformStamped_B = geometry_msgs.msg.TransformStamped()
                static_transformStamped_B.header.stamp = rospy.Time.now()
                static_transformStamped_B.header.frame_id = self.robot_base_ref
                static_transformStamped_B.child_frame_id = self.robot_pose_ref
                static_transformStamped_B.transform = self.tU.L2T(L)
                self.tU.tfToStaticTS(static_transformStamped_B)
                # self.M_br_ee = self.tU.L2M(L)

                self.stateMachineState+=1

        elif self.stateMachineState == 3:
            ### Applique back_to_origin
            self.back_to_origin_management()

            if self.originState == -1:
                if self.instruct in ["IMU","Opti"]:
                    ## first, create static transform for optitrack base (to avoid any bugs)
                    msg = "%s#%s"%(self.global_ref,self.opti_base_ref)
                    suc = self.tU.tfToStaticStr(msg)
                    if suc:
                        self.stateMachineState+=1   
                        self.begTime = rospy.Time.now().to_sec()                 

                elif self.instruct in ["simMark"]:
                    self.stateMachineState+=4
        elif self.stateMachineState == 4:
            ## special case for IMU and Opti, where we want to set their origin orientation
            ## such as world => Mdes_0_frame = world => control_frame  
            ## As we want the correction to be as good as possible, let's wait for the speed
            ## to be as close to zero as possible

            # thresTime = 5.0
            # if (rospy.Time.now().to_sec() - self.begTime) > thresTime:
            #      self.stateMachineState+=1  

            thresVal = 1e-4
            if (np.max(self.curSpeed)) < thresVal:
                 print(self.curSpeed)
                 self.takeSpeed = False
                 self.stateMachineState+=1  


        elif self.stateMachineState == 5:
            ## special case for IMU and Opti, where we want to set their origin orientation
            ## such as world => Mdes_0_frame = world => control_frame  

            msg = String("%s/%s/%s"%(self.instruct_frame,self.master_SM_ref,self.slave_side_ref))
            self.gotCorrConfirm = False
            self.corrComPub.publish(msg)
            self.stateMachineState+=1                   

        elif self.stateMachineState == 6:
            ## wait for confirm of correction
            msg = String("%s/%s/%s"%(self.instruct_frame,self.master_SM_ref,self.slave_side_ref))
            self.corrComPub.publish(msg)
       
            if self.gotCorrConfirm:
                self.stateMachineState+=1


        elif self.stateMachineState == 7:

            ## réautorise contrôle cartésien

            req = SetFloatRequest()
            req.data.data = 0.0
            success,resp1,ret = self.cM.call_service("set_controller", self.set_control_type,req)            
            if success:
                self.concludeState()


    def set_state(self,msg):

        if (msg.data in self.possible_states):
            print(msg.data)
            if self.state == "quitBoundaries" and msg.data != self.state:

                if self.originState >= 1:
                    self.originState = 5
                    self.needQBClean = True
                    self.savedState = msg.data
                
                elif self.stateMachineState >= 1:
                    self.stateMachineState = 3
                    self.needQBClean = True
                    self.savedState = msg.data
                
                else:
                    ## can react normally, no huge consequences

                    self.stateMachineState = 0
                    self.state = msg.data                    

            

            else:
                if (msg.data == self.state):
                    print("Rebooting state")

                if msg.data == "Reboot" and self.state == "Stopped":
                    ## do nothing
                    a = 1
                

                    

                else:
                    self.stateMachineState = 0
                self.state = msg.data
            return(True)
        else:
            print("Not a possible state. Possible states are : ")
            for state in self.possible_states:
                print(state + " ")
            return(False)



if __name__ == "__main__":
    gimbalControllerManagement()