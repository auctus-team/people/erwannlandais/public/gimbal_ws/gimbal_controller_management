# Description

This package contains different state machines, managing different situations and interactions with user.

3 states machines are available : 

* gimbal_controller_management.py : the principal state machine. It manages the robot's start-up, its return to rest configuration (via “Sleep”), or its return to initial configuration (via a click on the green interface button). It also manages the procedure for returning the first joint's return to the center of the joint limits, by exploiting the singularity.
* check_boundaries_center.py: a state machine whose role is to check whether a joint center reset situation should occur. If this happens, this state machine tells gimbal_controller_management to apply the appropriate procedure.
* rosservice_server_manager.py (used in the second experiment): a state machine whose role is to manage various processes specific to the experiment (activating or deactivating the visual display, recording the time spent using each of MV tools).