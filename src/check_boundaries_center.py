#!/usr/bin/env python3

import geometry_msgs.msg
from math import pi
from std_msgs.msg import String, MultiArrayDimension, Float64

from std_srvs.srv import SetBool

import rospy

import actionlib
import math

import numpy as np

from msg_srv_action_gestion.srv import GetFloat, GetFloatRequest, GetFloatResponse, SetString, GetBool, GetBoolRequest, DoubleFloat64MultiArraySrv, DoubleFloat64MultiArraySrvRequest, Float64MultiArraySrv, Float64MultiArraySrvRequest, SetString, SetStringRequest
from msg_srv_action_gestion.msg import poseVelAcc

from std_srvs.srv import SetBool

from sensor_msgs.msg import JointState

import tf2_ros
import geometry_msgs.msg

import roslaunch

from tf_management_pkg.tf_utils import tfUtils

from ros_wrap.com_manager import comManager


class checkBoundaries:
    """!
    
    Only goal of node : 

        - Take angles between 2 vectors (from topic)
        - Take current articulation angles
        - If : 
            * State (from /get_manager_state) = "IDLE"
            * And : 
                - abs(1e arti) > 4.0 and angles < 0.5 rad
                - abs(1e arti) > 6.2
            
            ==> Send trigger for "quitBoundaries" state.

    """
    def get_timer_call(self,msg):
        if self.inRecord:
            self.state = "RebootRec"
        else:
            self.state = "StartRec"
        self.SMS = 0
        self.activeTimer = True

    def get_timeout(self,req):

        self.state = "timeoutStop"
        self.SMS = 0

        return(True)

    def get_gcm_state(self,msg):

        self.gcm_state = msg.data


    def get_angle(self,msg):
        self.cur_ang = msg.data

    def get_joint_state(self,msg):
        
        for k in range(len(msg.name)):
            if msg.name[k][-1] == "1":
                self.jv1 = msg.position[k]

    def __init__(self):
        a = 0
        rospy.init_node('check_boundaries_call',
                    anonymous=True)
        nodeName = rospy.get_name()
        nodeName+="/"
        self.r = rospy.Rate(10.0)
        self.isSim = rospy.get_param(nodeName + "sim", True)

        self.cM = comManager()

        ## Publishers
            
        self.pub_stop_time = rospy.Publisher("/timer_infos", Float64)

        ## Subscribers

        self.curStatePub = rospy.Subscriber("/get_manager_state",String, self.get_gcm_state)
        self.gcm_state = ""

        self.curAngles = rospy.Subscriber("/angle_centerBoundTF_to_vM_frame", Float64, self.get_angle)

        self.cur_ang = 3.14
        
        if self.isSim:
            self.get_joint_value_1= rospy.Subscriber("/gimbal/joint_states",JointState, self.get_joint_state)

        else:
            self.get_joint_value_1 = rospy.Subscriber("/joint_states",JointState, self.get_joint_state)
        
        self.jv1 = 0.0

        ## threshold angle (in radians)
        self.thresAng =  rospy.get_param(nodeName + "thresAng", 0.5)

        ## threshold articular value (in radians)
        self.thresArti = rospy.get_param(nodeName + "artiAngle", 4.0)

        ## Clients

        self.state_change = rospy.ServiceProxy('/management_state',SetString)




        while not rospy.is_shutdown():

            if self.gcm_state == "IDLE":
                p1 = ( abs(self.cur_ang) < self.thresAng and abs(self.jv1) > self.thresArti )
                p2 = abs(self.jv1) > 6.2
                if p1 or p2:
                    req = SetStringRequest()
                    req.data = "quitBoundaries"
                    suc,resp,ret = self.cM.call_service("management_state",self.state_change,req)

            self.r.sleep()
        




if __name__ == "__main__":
    checkBoundaries()